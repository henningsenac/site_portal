function usuariosModels(connection) {

    this._connection = connection;

    usuariosModels.prototype.getUsuarios = function (callback) {
        this._connection.query('select * from usuarios', callback);
    }

    usuariosModels.prototype.getUsuarioId = function (callback) {
        this._connection.query('select * from usuarios where id_usuarios = 1', callback);
    }

    usuariosModels.prototype.postUsuario = function (usuario, callback) {
        console.log('data: ', usuario)
        this._connection.query('insert into usuarios set ?', usuario, callback);
    }
}

module.exports = function () {
    return usuariosModels;
}