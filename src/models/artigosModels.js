function artigosModels(connection){

	this._connection = connection;

	artigosModels.prototype.getArtigos = function(callback){
		this._connection.query('select * from artigos', callback);
	}

	artigosModels.prototype.getArtigoId = function(id_artigo, callback){
		console.log('id artigoMD:',  id_artigo.id_artigo)
		this._connection.query('select * from artigos where id_artigos = '+ id_artigo.id_artigo, callback);
	}
	
	artigosModels.prototype.postArtigo = function(artigo, callback){
		console.log('postArtigo no artigosModels.js: ', artigo)
		this._connection.query('insert into artigos set ?', artigo, callback);
	}

	artigosModels.prototype.putArtigo = function(artigo, id_artigo, callback){
		console.log('updateArtigo no artigoModels.js: ', artigo, id_artigo.id_artigo)
		this._connection.query(`update artigos set ${artigo} where id_artigos = ${id_artigo.id_artigo}`, callback);
		
	}
}

module.exports = function(){
	return artigosModels;
}