module.exports = function(app) { // Exporta a função passando como parâmetro a variável 'app'

    app.get('/', function(req, res) { // Traz a 'req' e a 'res' para a raiz do projeto
        app.src.controllers.indexCT.index_CT(app, req, res) // Chama o arquivo 'indexCT' acessando o método indexCT 'app', 'req' e 'res'
    })



    app.get('/home', function(req, res) { // Traz a 'req' e a 'res' para a página home
        app.src.controllers.indexCT.cards_CT(app, req, res) // Chama o arquivo 'indexCT' acessando o método homeCT 'app', 'req' e 'res'

    })





}