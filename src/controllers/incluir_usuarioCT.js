var path = require('path');
var multer = require("multer");

module.exports.incluir_usuario = function (app, req, res) {
    res.render('incluir_usuario')
}

module.exports.salvar_usuario = function (app, req, res) {

    // definição dos parametros de armazenamento
    var storage = multer.diskStorage({
        destination: './public/uploads/',
        filename: function (req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    });

    var upload = multer({
        storage: storage,
        limits: { fileSize: 8000000 }
    }).single('imagem_usuario');

    upload(req, res, function (err) {

        const { filename, size } = req.file ? req.file : "";
        console.log('nome: ', filename, ' - ', 'tamanho: ', size)
        if (err) {
            console.log(err)
        }
        console.log('upload: ', req.file)
        
        var connection = app.config.dbConnection();
        var usuariosModels = new app.src.models.usuariosModels(connection);


        var { nome, descricao, funcao_projetos, email, senha, imagem_usuario } = req.body;

        console.log('velho req: ', req.body)

        var usuario = {
            nome,
            descricao,
            funcao_projetos,
            email,
            senha,
            imagem_usuario: filename ? `/uploads/${filename}` : ''
        }

        req.body = usuario

        console.log('novo req: ', req.body)

        req.assert('nome', 'Nome deve ser informado').notEmpty()
        req.assert('descricao', 'A descrição deve ser informada').notEmpty()
        req.assert('funcao_projetos', 'A função do projeto deve ser informada').notEmpty()
        req.assert('email', 'Deve ser informado um email valido').notEmpty()
        req.assert('senha', 'Senha não pode ser vazia').notEmpty()
        req.assert('senha', 'Informar uma senha igual ou maior que 6 digitos').len(6, 8)
        req.assert('imagem_usuario', 'A imagem tem que ser adicionada').notEmpty()

        var erros = req.validationErrors()

        console.log('Erros_usuario:', erros)

        if (erros) {
            res.send({ validacao: erros })
            return
        }

        // console.log('dadosdousuario: ', usuario);

        usuariosModels.postUsuario(usuario, function (error, result) {
            console.log('cheguei', result);
            res.send({ validacao: "salvei" });
        })
    })
}