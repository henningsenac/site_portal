var express = require('express') // Aqui eu importo o 'express'
var consign = require('consign') // Aqui eu importo o 'consign'
var bodyParser = require('body-parser') // Aqui eu importo o 'body-parser'
var expressValidator = require('express-validator') // Aqui eu importo o 'express-validator'


var app = express() // Cria a instância do 'express' na variável 'app'

app.use(bodyParser.urlencoded({extended:true})) // Faz a variável 'app' utilizar o 'bodyParser'
app.use(expressValidator())

app.set('view engine', 'ejs') // Setamos que a engine será o 'ejs'
app.set('views','./views') // Setamos a pasta 'views'
app.use(express.static('./public')) // Indica o caminho da pasta onde estão os arquivos estáticos
app.use("/uploads", express.static('./public/uploads/')); // Indica o caminho da pasta dos arquivos de 'upload'

consign() // Carrega o módulo 'consign'
    .include('src/routes') // Inclui o caminho da pasta 'routes'
    .then('src/controllers') // Indica o caminho dos arquivos de 'controllers'
    .then('src/models') // Indica o caminho dos arquivos de 'models'
    .then('config/dbConnection.js') // Indica o caminho para conexão com o banco de dados
    .into(app) // Injeta na variável 'app' todas as funções do 'consign'


module.exports = app // Chama a função 'app'