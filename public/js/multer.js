

const fileInput = document.querySelector('#file-js-example input[type=file]');
    fileInput.onchange = () => {
      if (fileInput.files.length > 0) {
        const fileName = document.querySelector('#file-js-example .file-name');
        fileName.textContent = fileInput.files[0].name;
        const submitButton = document.querySelector('#file-js-submit');
        submitButton.removeAttribute('disabled')
      }
    }

    function readImage() {
      if (this.files && this.files[0]) {
        var file = new FileReader();
        file.onload = function (e) {
          document.getElementById("imagem").src = e.target.result;
        };
        file.readAsDataURL(this.files[0]);
      }
    }
    document.getElementById("inputImg").addEventListener("change", readImage, false);