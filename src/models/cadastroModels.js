function usuariosModels(connection) {

    this._connection = connection;

    usuariosModels.prototype.getUsuarios = function (callback) {
        this._connection.query('select * from usuarios', callback);
    }

    usuariosModels.prototype.getUsuarioId = function (callback) {
        this._connection.query('select * from usuarios where id_cadastros = 1', callback);
    }

    usuariosModels.prototype.postUsuario = function (cadastro, callback) {
        console.log('data: ', cadastro)
        this._connection.query('insert into usuarios set ?', cadastro, callback);
    }
}

module.exports = function () {
    return usuariosModels;
}