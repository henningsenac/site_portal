const body = document.querySelector("body")
const escurecerFundo = document.querySelectorAll(".escurecer-fundo")
const escurecerTexto = document.querySelectorAll(".escurecer-texto")
const alavanca = document.querySelector("input[name=alavanca]")
const alavancaM = document.querySelector("input[name=alavancaM]")

const modoEscuro = () => {
  body.style.setProperty("background-color", "#333333")
  body.style.setProperty("color", "#B5B5B5")
  for(var i = 0;i < escurecerFundo.length;i++){
    escurecerFundo[i].style.setProperty("background-color", "#494646")}
  for(var i = 0;i < escurecerTexto.length;i++){
    escurecerTexto[i].style.setProperty("color", "#B5B5B5")}
}

const modoNormal = () => {
  body.style.setProperty("background-color", "#F5FFFA")
  body.style.setProperty("color", "#000080")
  for(var i = 0;i < escurecerFundo.length;i++){
    escurecerFundo[i].style.setProperty("background-color", "#F0F8FF")}
  for(var i = 0;i < escurecerTexto.length;i++){
    escurecerTexto[i].style.setProperty("color", "#000080")}
}

alavanca.addEventListener('change', ({ target }) => {
  target.checked ? modoEscuro() : modoNormal()
})

alavancaM.addEventListener('change', ({ target }) => {
  target.checked ? modoEscuro() : modoNormal()
})

alavanca.addEventListener('change', ({ target }) => {
  target.checked ? alavancaM.checked = true : alavancaM.checked = false
})

alavancaM.addEventListener('change', ({ target }) => {
  target.checked ? alavanca.checked = true : alavanca.checked = false
})