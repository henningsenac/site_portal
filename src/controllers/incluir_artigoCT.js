var path = require('path');
var multer = require("multer");


module.exports.incluir_artigo = function(app, req, res) {
    res.render("incluir_artigo");
}

module.exports.editar_artigo = function(app, req, res) {
    var connection = app.config.dbConnection();
    var artigosModels = new app.src.models.artigosModels(connection);
    var id_artigo = req.query;

    artigosModels.getArtigoId(id_artigo, function(error, result) {

            console.log('getArtigos no banco', result);
            res.render("editar_artigo", { data: result });
        })
        // var data = {
        //     titulo: 'html',
        //     descricao: 'html',
        //     conteudo: 'html e uama',
        //     autor: 'gaston',
        //     data_criacao: '',
        //     data_modificacao: '',
        //     referencia: 'teste',
        //     imagem_artigo:''
        // }
        // console.log(data);
        // res.render("editar_artigo", { data: data });

}


module.exports.salvar_artigo = function(app, req, res) {

    // definição dos parametros de armazenamento
    var storage = multer.diskStorage({
        destination: './public/uploads/',
        filename: function(req, file, cb) {
            cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
        }
    });

    //definição da função de upload
    var upload = multer({
        storage: storage,
        limits: { fileSize: 8000000 }
    }).single('artigo');

    //chamada da função de upload
    upload(req, res, function(err) {
        const { filename, size } = req.file ? req.file : "";
        console.log('nome: ', filename, ' - ', 'tamanho: ', size)
        if (err) {
            console.log(err)
        }
        console.log('upload: ', req.file)

        var connection = app.config.dbConnection();
        var artigosModels = new app.src.models.artigosModels(connection);


        var { titulo, descricao, conteudo, autor, data_criacao, data_modificacao, referencia } = req.body;
        var d = new Date();

        var day = d.getDate();
        var month = d.getMonth() + 1;
        var year = d.getFullYear();

        var artigo = {
            titulo,
            descricao,
            conteudo,
            autor,
            data_criacao: day + '/' + month + '/' + year,
            data_modificacao: day + '/' + month + '/' + year,
            referencia,
            imagem_artigo: filename ? `/uploads/${filename}` : ''
        }

        req.body = artigo

        console.log('novo req: ', req.body)

        req.assert('titulo', 'O titulo deve ser informado').notEmpty()
        req.assert('descricao', 'A descrição deve ser informada').notEmpty()
        req.assert('conteudo', 'O conteúdo deve ser informado').notEmpty()
        req.assert('autor', 'Deve ser informado um autor').notEmpty()
        req.assert('referencia', 'A referência deve ser informada').notEmpty()
        req.assert('imagem_artigo', 'A imagem tem que ser adicionada').notEmpty()

        var erros = req.validationErrors()

        console.log('Erros usuario:', erros)

        if (erros) {
            res.send({ validacao: erros })
            return
        }

        artigosModels.postArtigo(artigo, function(error, result) {
            res.send({ validacao: "salvei" })
            console.log('postArtigo no incluir_artigoCT.js', result);
        })
    })
}

module.exports.update_artigo = function(app, req, res) {

    // definição dos parametros de armazenamento
    // var storage = multer.diskStorage({
    //     destination: './public/uploads/',
    //     filename: function (req, file, cb) {
    //         cb(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname));
    //     }
    // });

    //definição da função de upload
    // var upload = multer({
    //     storage: storage,
    //     limits: { fileSize: 8000000 }
    // }).single('artigo');

    //chamada da função de upload
    // upload(req, res, function (err) {
    //     const { filename, size } = req.file;
    //     console.log('nome: ', filename, ' - ', 'tamanho: ', size)
    //     if (err) {
    //         console.log(err)
    //     }
    //     console.log('upload: ', req.file)
    //  res.render("avatar", { image: `/uploads/${filename}`, size });

    var connection = app.config.dbConnection();
    var artigosModels = new app.src.models.artigosModels(connection);

    var { titulo, descricao, conteudo, autor, data_criacao, data_modificacao, referencia } = req.body;
    var artigo = {
        titulo,
        descricao,
        conteudo,
        autor,
        data_criacao,
        data_modificacao,
        referencia,
        // imagem_artigo: `/uploads/${filename}`
    }
    artigosModels.putArtigo(artigo, function(error, result) {
        res.send({ data: artigo })
        console.log('postArtigo no incluir_artigoCT.js', result);
    })

}