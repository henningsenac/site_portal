module.exports = function (app) { // Exporta a função passando como parâmetro a variável 'app'
    // app.get('/individual', function (req, res) {
    //     app.src.controllers.listagemCT.listagem_run(app, req, res)
    // })

    app.get('/listagem', function (req, res) { // Traz a 'req' e a 'res' para a página listagem
        app.src.controllers.listagemCT.listagem_run(app, req, res) // Chama o arquivo 'listagemCT' acessando o método listagem_run 'app', 'req' e 'res'
    })
}