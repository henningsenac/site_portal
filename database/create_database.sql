CREATE DATABASE IF NOT EXISTS portal_web;
USE portal_web;
CREATE TABLE IF NOT EXISTS artigos(
    id_artigos INT NOT NULL AUTO_INCREMENT,
    titulo VARCHAR(50) NOT NULL,
    descricao VARCHAR(100) NOT NULL,
    conteudo LONGTEXT NOT NULL,
    autor VARCHAR(45) NOT NULL,
    data_criacao VARCHAR(255), 
    data_modificacao VARCHAR(255),
    referencia VARCHAR(200) NOT NULL,
    imagem_artigo VARCHAR(255),
    PRIMARY KEY(id_artigos));

CREATE TABLE IF NOT EXISTS usuarios (
    id_usuarios INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(50) NOT NULL,
    descricao VARCHAR(100) NOT NULL,
    funcao_projetos VARCHAR(30) NOT NULL,
    email VARCHAR(100),
    senha VARCHAR(50), 
    imagem_usuario VARCHAR(255),
    PRIMARY KEY (id_usuarios)
);

INSERT INTO artigos(
    titulo, descricao, conteudo, autor, data_criacao, data_modificacao, referencia
)
VALUES
    ('este é meu titulo', 'esta é minha descricao', 'este é meu conteudo', 'este é meu autor', '12/12/2012', '13/12/2012', 'esta é minha referencia'),
    ('este é meu titulo1', 'esta é minha descricao1', 'este é meu conteudo1', 'este é meu autor1', '13/12/2012', '14/12/2012', 'esta é minha referencia1'),
    ('este é meu titulo2', 'esta é minha descricao2', 'este é meu conteudo2', 'este é meu autor2', '14/12/2012', '15/12/2012', 'esta é minha referencia2');

INSERT INTO usuarios(
    nome, descricao, funcao_projetos, email, senha, imagem_usuario
)
VALUES
    ('este é meu nome', 'esta é minha descricao', 'este é minha funcao', 'este é meu email', 'esta é minha senha', 'essa é minha imagem'),
    ('este é meu nome1', 'esta é minha descricao1', 'este é minha funcao1', 'este é meu email1', 'esta é minha senha1', 'essa é minha imagem1'),
    ('este é meu nome2', 'esta é minha descricao2', 'este é minha funcao2', 'este é meu email2', 'esta é minha senha2', 'essa é minha imagem2');
